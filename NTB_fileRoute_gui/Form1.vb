Imports System.Xml
Imports System.Configuration.ConfigurationSettings

Public Class Form1

    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Route1 As NTB_fileRoute_gui.route
    Friend WithEvents DataGrid1 As System.Windows.Forms.DataGrid
    Friend WithEvents DataGridTableStyle1 As System.Windows.Forms.DataGridTableStyle
    Friend WithEvents DataGridTextBoxColumn2 As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents DataGridBoolColumn1 As System.Windows.Forms.DataGridBoolColumn
    Friend WithEvents DataGridTextBoxColumn3 As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents DataGridTextBoxID As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents DataGridTextBoxColumn4 As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents DataGridBoolColumn2 As System.Windows.Forms.DataGridBoolColumn
    Friend WithEvents DataGridTextBoxColumn1 As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents DataGridTextBoxColumn5 As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents DataGridTextBoxColumn6 As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents DataGridTableStyle2 As System.Windows.Forms.DataGridTableStyle
    Friend WithEvents DataGridTextBoxColumn7 As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents DataGridTextBoxColumn8 As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents DataGridTextBoxColumn9 As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents DataGridBoolColumn3 As System.Windows.Forms.DataGridBoolColumn
    Friend WithEvents DataGridBoolColumn4 As System.Windows.Forms.DataGridBoolColumn
    Friend WithEvents DataGridTextBoxColumn10 As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents DataGridTextBoxColumn11 As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents DataGridTextBoxColumn12 As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents DataGridBoolColumn5 As System.Windows.Forms.DataGridBoolColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(Form1))
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.Route1 = New NTB_fileRoute_gui.route
        Me.DataGrid1 = New System.Windows.Forms.DataGrid
        Me.DataGridTableStyle1 = New System.Windows.Forms.DataGridTableStyle
        Me.DataGridTextBoxID = New System.Windows.Forms.DataGridTextBoxColumn
        Me.DataGridTextBoxColumn1 = New System.Windows.Forms.DataGridTextBoxColumn
        Me.DataGridBoolColumn1 = New System.Windows.Forms.DataGridBoolColumn
        Me.DataGridTextBoxColumn2 = New System.Windows.Forms.DataGridTextBoxColumn
        Me.DataGridTextBoxColumn3 = New System.Windows.Forms.DataGridTextBoxColumn
        Me.DataGridBoolColumn2 = New System.Windows.Forms.DataGridBoolColumn
        Me.DataGridTextBoxColumn4 = New System.Windows.Forms.DataGridTextBoxColumn
        Me.DataGridTextBoxColumn5 = New System.Windows.Forms.DataGridTextBoxColumn
        Me.DataGridTextBoxColumn6 = New System.Windows.Forms.DataGridTextBoxColumn
        Me.DataGridTableStyle2 = New System.Windows.Forms.DataGridTableStyle
        Me.DataGridTextBoxColumn7 = New System.Windows.Forms.DataGridTextBoxColumn
        Me.DataGridTextBoxColumn8 = New System.Windows.Forms.DataGridTextBoxColumn
        Me.DataGridTextBoxColumn9 = New System.Windows.Forms.DataGridTextBoxColumn
        Me.DataGridBoolColumn3 = New System.Windows.Forms.DataGridBoolColumn
        Me.DataGridBoolColumn4 = New System.Windows.Forms.DataGridBoolColumn
        Me.DataGridTextBoxColumn10 = New System.Windows.Forms.DataGridTextBoxColumn
        Me.DataGridTextBoxColumn11 = New System.Windows.Forms.DataGridTextBoxColumn
        Me.DataGridTextBoxColumn12 = New System.Windows.Forms.DataGridTextBoxColumn
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.DataGridBoolColumn5 = New System.Windows.Forms.DataGridBoolColumn
        CType(Me.Route1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(472, 552)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(128, 24)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Save"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(176, 552)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(128, 24)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "Expand"
        '
        'Route1
        '
        Me.Route1.DataSetName = "route"
        Me.Route1.EnforceConstraints = False
        Me.Route1.Locale = New System.Globalization.CultureInfo("nb-NO")
        '
        'DataGrid1
        '
        Me.DataGrid1.CaptionText = "NTB FileRoute Config"
        Me.DataGrid1.DataMember = ""
        Me.DataGrid1.DataSource = Me.Route1.job
        Me.DataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.DataGrid1.Location = New System.Drawing.Point(8, 8)
        Me.DataGrid1.Name = "DataGrid1"
        Me.DataGrid1.Size = New System.Drawing.Size(1056, 528)
        Me.DataGrid1.TabIndex = 2
        Me.DataGrid1.TableStyles.AddRange(New System.Windows.Forms.DataGridTableStyle() {Me.DataGridTableStyle1, Me.DataGridTableStyle2})
        '
        'DataGridTableStyle1
        '
        Me.DataGridTableStyle1.DataGrid = Me.DataGrid1
        Me.DataGridTableStyle1.GridColumnStyles.AddRange(New System.Windows.Forms.DataGridColumnStyle() {Me.DataGridTextBoxID, Me.DataGridTextBoxColumn1, Me.DataGridBoolColumn1, Me.DataGridTextBoxColumn2, Me.DataGridTextBoxColumn3, Me.DataGridBoolColumn2, Me.DataGridTextBoxColumn4, Me.DataGridTextBoxColumn5, Me.DataGridTextBoxColumn6})
        Me.DataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.DataGridTableStyle1.MappingName = "job"
        Me.DataGridTableStyle1.PreferredColumnWidth = 200
        Me.DataGridTableStyle1.PreferredRowHeight = 20
        '
        'DataGridTextBoxID
        '
        Me.DataGridTextBoxID.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.DataGridTextBoxID.Format = ""
        Me.DataGridTextBoxID.FormatInfo = Nothing
        Me.DataGridTextBoxID.HeaderText = "ID"
        Me.DataGridTextBoxID.MappingName = "id"
        Me.DataGridTextBoxID.ReadOnly = True
        Me.DataGridTextBoxID.Width = 30
        '
        'DataGridTextBoxColumn1
        '
        Me.DataGridTextBoxColumn1.Format = ""
        Me.DataGridTextBoxColumn1.FormatInfo = Nothing
        Me.DataGridTextBoxColumn1.HeaderText = "Jobb Navn"
        Me.DataGridTextBoxColumn1.MappingName = "name"
        Me.DataGridTextBoxColumn1.Width = 200
        '
        'DataGridBoolColumn1
        '
        Me.DataGridBoolColumn1.FalseValue = "false"
        Me.DataGridBoolColumn1.HeaderText = "Disabled"
        Me.DataGridBoolColumn1.MappingName = "disabled"
        Me.DataGridBoolColumn1.NullValue = CType(resources.GetObject("DataGridBoolColumn1.NullValue"), Object)
        Me.DataGridBoolColumn1.TrueValue = "true"
        Me.DataGridBoolColumn1.Width = 40
        '
        'DataGridTextBoxColumn2
        '
        Me.DataGridTextBoxColumn2.Format = ""
        Me.DataGridTextBoxColumn2.FormatInfo = Nothing
        Me.DataGridTextBoxColumn2.HeaderText = "Input Path"
        Me.DataGridTextBoxColumn2.MappingName = "inputpath"
        Me.DataGridTextBoxColumn2.Width = 200
        '
        'DataGridTextBoxColumn3
        '
        Me.DataGridTextBoxColumn3.Format = ""
        Me.DataGridTextBoxColumn3.FormatInfo = Nothing
        Me.DataGridTextBoxColumn3.HeaderText = "Filter"
        Me.DataGridTextBoxColumn3.MappingName = "filter"
        Me.DataGridTextBoxColumn3.Width = 50
        '
        'DataGridBoolColumn2
        '
        Me.DataGridBoolColumn2.FalseValue = "false"
        Me.DataGridBoolColumn2.HeaderText = "FTP"
        Me.DataGridBoolColumn2.MappingName = "ftp"
        Me.DataGridBoolColumn2.NullValue = CType(resources.GetObject("DataGridBoolColumn2.NullValue"), Object)
        Me.DataGridBoolColumn2.TrueValue = "true"
        Me.DataGridBoolColumn2.Width = 40
        '
        'DataGridTextBoxColumn4
        '
        Me.DataGridTextBoxColumn4.Format = ""
        Me.DataGridTextBoxColumn4.FormatInfo = Nothing
        Me.DataGridTextBoxColumn4.HeaderText = "Username"
        Me.DataGridTextBoxColumn4.MappingName = "username"
        Me.DataGridTextBoxColumn4.Width = 75
        '
        'DataGridTextBoxColumn5
        '
        Me.DataGridTextBoxColumn5.Format = ""
        Me.DataGridTextBoxColumn5.FormatInfo = Nothing
        Me.DataGridTextBoxColumn5.HeaderText = "Password"
        Me.DataGridTextBoxColumn5.MappingName = "password"
        Me.DataGridTextBoxColumn5.Width = 75
        '
        'DataGridTextBoxColumn6
        '
        Me.DataGridTextBoxColumn6.Format = ""
        Me.DataGridTextBoxColumn6.FormatInfo = Nothing
        Me.DataGridTextBoxColumn6.HeaderText = "Directory"
        Me.DataGridTextBoxColumn6.MappingName = "directory"
        Me.DataGridTextBoxColumn6.Width = 75
        '
        'DataGridTableStyle2
        '
        Me.DataGridTableStyle2.AllowSorting = False
        Me.DataGridTableStyle2.DataGrid = Me.DataGrid1
        Me.DataGridTableStyle2.GridColumnStyles.AddRange(New System.Windows.Forms.DataGridColumnStyle() {Me.DataGridTextBoxColumn7, Me.DataGridTextBoxColumn8, Me.DataGridTextBoxColumn9, Me.DataGridBoolColumn3, Me.DataGridBoolColumn5, Me.DataGridBoolColumn4, Me.DataGridTextBoxColumn10, Me.DataGridTextBoxColumn11, Me.DataGridTextBoxColumn12})
        Me.DataGridTableStyle2.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.DataGridTableStyle2.MappingName = "output"
        '
        'DataGridTextBoxColumn7
        '
        Me.DataGridTextBoxColumn7.Alignment = System.Windows.Forms.HorizontalAlignment.Center
        Me.DataGridTextBoxColumn7.Format = ""
        Me.DataGridTextBoxColumn7.FormatInfo = Nothing
        Me.DataGridTextBoxColumn7.HeaderText = "ID"
        Me.DataGridTextBoxColumn7.MappingName = "id"
        Me.DataGridTextBoxColumn7.Width = 30
        '
        'DataGridTextBoxColumn8
        '
        Me.DataGridTextBoxColumn8.Format = ""
        Me.DataGridTextBoxColumn8.FormatInfo = Nothing
        Me.DataGridTextBoxColumn8.HeaderText = "Tekst"
        Me.DataGridTextBoxColumn8.MappingName = "text"
        Me.DataGridTextBoxColumn8.Width = 200
        '
        'DataGridTextBoxColumn9
        '
        Me.DataGridTextBoxColumn9.Format = ""
        Me.DataGridTextBoxColumn9.FormatInfo = Nothing
        Me.DataGridTextBoxColumn9.HeaderText = "Ut path / IP-nr"
        Me.DataGridTextBoxColumn9.MappingName = "path"
        Me.DataGridTextBoxColumn9.Width = 200
        '
        'DataGridBoolColumn3
        '
        Me.DataGridBoolColumn3.FalseValue = "false"
        Me.DataGridBoolColumn3.HeaderText = "Disabled"
        Me.DataGridBoolColumn3.MappingName = "disabled"
        Me.DataGridBoolColumn3.NullValue = CType(resources.GetObject("DataGridBoolColumn3.NullValue"), Object)
        Me.DataGridBoolColumn3.TrueValue = "true"
        Me.DataGridBoolColumn3.Width = 40
        '
        'DataGridBoolColumn4
        '
        Me.DataGridBoolColumn4.FalseValue = "false"
        Me.DataGridBoolColumn4.HeaderText = "FTP"
        Me.DataGridBoolColumn4.MappingName = "ftp"
        Me.DataGridBoolColumn4.NullValue = CType(resources.GetObject("DataGridBoolColumn4.NullValue"), Object)
        Me.DataGridBoolColumn4.TrueValue = "true"
        Me.DataGridBoolColumn4.Width = 40
        '
        'DataGridTextBoxColumn10
        '
        Me.DataGridTextBoxColumn10.Format = ""
        Me.DataGridTextBoxColumn10.FormatInfo = Nothing
        Me.DataGridTextBoxColumn10.HeaderText = "username"
        Me.DataGridTextBoxColumn10.MappingName = "username"
        Me.DataGridTextBoxColumn10.Width = 75
        '
        'DataGridTextBoxColumn11
        '
        Me.DataGridTextBoxColumn11.Format = ""
        Me.DataGridTextBoxColumn11.FormatInfo = Nothing
        Me.DataGridTextBoxColumn11.HeaderText = "Password"
        Me.DataGridTextBoxColumn11.MappingName = "password"
        Me.DataGridTextBoxColumn11.Width = 75
        '
        'DataGridTextBoxColumn12
        '
        Me.DataGridTextBoxColumn12.Format = ""
        Me.DataGridTextBoxColumn12.FormatInfo = Nothing
        Me.DataGridTextBoxColumn12.HeaderText = "Directory"
        Me.DataGridTextBoxColumn12.MappingName = "directory"
        Me.DataGridTextBoxColumn12.Width = 75
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(320, 552)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(128, 24)
        Me.Button3.TabIndex = 3
        Me.Button3.Text = "<- tilbake til job"
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(24, 552)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(128, 24)
        Me.Button4.TabIndex = 4
        Me.Button4.Text = "&Editer"
        '
        'DataGridBoolColumn5
        '
        Me.DataGridBoolColumn5.FalseValue = "false"
        Me.DataGridBoolColumn5.HeaderText = "DateSubDir"
        Me.DataGridBoolColumn5.MappingName = "datesub"
        Me.DataGridBoolColumn5.NullValue = CType(resources.GetObject("DataGridBoolColumn5.NullValue"), Object)
        Me.DataGridBoolColumn5.TrueValue = "true"
        Me.DataGridBoolColumn5.Width = 75
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(1072, 590)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.DataGrid1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.Route1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private xmlFile As String = AppSettings("xmlFileName")
    Private xmlDoc As New XmlDocument

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Route1.ReadXml(xmlFile)
        Me.DataGrid1.ReadOnly = True
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If MsgBox("Vil du lagre n�?", MsgBoxStyle.YesNo Or MsgBoxStyle.DefaultButton2 Or MsgBoxStyle.Exclamation) = MsgBoxResult.Yes Then
            Me.Route1.WriteXml(xmlFile & "_ds2.xml") ', XmlWriteMode.DiffGram)
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Route1.job.AddjobRow("test", "Path", "Filter", "FTP", "usr", "pass", "dir", "55", "diS")
        Static expanded As Boolean

        If expanded Then
            expanded = False
            Me.DataGrid1.Collapse(-1)
        Else
            expanded = True
            Me.DataGrid1.Expand(-1)
        End If

    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.DataGrid1.ReadOnly = Not Me.DataGrid1.ReadOnly
        If Me.DataGrid1.ReadOnly Then
            Button4.Text = "&Editer"
        Else
            Button4.Text = "&L�s"
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'Me.DataGrid1.NavigateTo(0, "job_output")
        Me.DataGrid1.NavigateBack()
    End Sub

End Class
